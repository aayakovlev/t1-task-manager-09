package ru.t1.aayakovlev.tm.component;

import ru.t1.aayakovlev.tm.constant.ArgumentConstant;
import ru.t1.aayakovlev.tm.constant.CommandConstant;
import ru.t1.aayakovlev.tm.controller.CommandController;
import ru.t1.aayakovlev.tm.controller.impl.CommandControllerImpl;
import ru.t1.aayakovlev.tm.repository.CommandRepository;
import ru.t1.aayakovlev.tm.repository.impl.CommandRepositoryImpl;
import ru.t1.aayakovlev.tm.service.CommandService;
import ru.t1.aayakovlev.tm.service.impl.CommandServiceImpl;

import java.util.Scanner;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.EMPTY_ARRAY_SIZE;
import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT;

public final class Bootstrap {

    private static final CommandRepository commandRepository = new CommandRepositoryImpl();

    private static final CommandService commandService = new CommandServiceImpl(commandRepository);

    private static final CommandController commandController = new CommandControllerImpl(commandService);

    public void run(final String[] args) {
        processArguments(args);
        processCommand();
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length == EMPTY_ARRAY_SIZE) return;
        String argument = arguments[FIRST_ARRAY_ELEMENT];
        processArgument(argument);
    }

    private void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;

        switch (argument) {
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showInfo();
                break;
            default:
                commandController.showArgumentError();
                break;
        }
    }

    private void processCommand() {
        System.out.println("**Welcome to Task Manager!**");
        Scanner scanner = new Scanner(System.in);
        String command;

        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("Enter command:");
            command = scanner.nextLine();

            switch (command) {
                case CommandConstant.ABOUT:
                    commandController.showAbout();
                    break;
                case CommandConstant.VERSION:
                    commandController.showVersion();
                    break;
                case CommandConstant.HELP:
                    commandController.showHelp();
                    break;
                case CommandConstant.INFO:
                    commandController.showInfo();
                    break;
                case CommandConstant.EXIT:
                    commandController.showExit();
                default:
                    commandController.showCommandError();
                    break;
            }
        }
    }

}
